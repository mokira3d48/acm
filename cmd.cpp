# include "cmd.h"

# include <iostream>
# include <string>
# include <vector>

/** Accesseur. */
std::string CMD::text() const { return this->_text; }
std::string CMD::program() const { return this->_program; }
std::vector<std::string>& CMD::arguments() { return this->_arguments; }

/** Muttateur. */
void CMD::program(std::string str) { this->_program = str; }

/** Constructor. */
CMD::CMD(std::string str) {
	_program = "";
	_text	 = "";

	if (str.compare("")) {
		std::vector<std::string> strs;
		int i = 0;
	
		this->split(str, strs);
		_text = str;


//		for ( ; i < strs.size(); i++)
//			std::cout << strs[i] << std::endl;

		if (!strs[i].compare("sudo")) {
			this->_program += "sudo ";
			i++;
		}

		this->_program += strs[i];
		i++;

		for (; i < strs.size(); i++)
			_arguments.push_back(strs[i]);
	}

}