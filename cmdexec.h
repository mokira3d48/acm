# ifndef CMDEXEC_H
# define CMDEXEC_H

# include "cmd.h"


class CMDExecutor {
	private :
		std::vector<CMD> 	_cmds;		/// Liste des commandes en attente d'exécution.
		std::vector<CMD> 	_ecmds;		/// Liste des commandes déjà exécutées.
		bool 				_run;		/// TRUE si c'est en cour d'exécution, FALSE sinon.

	public :

		/** Accesseur. */
		std::vector<CMD> getcmdQueue();
		std::vector<CMD> getcmdDone();

		/** Fonction qui permet d'ajouter une commande à la liste d'attente. */
		void add(CMD const&);

		/** Fonction qui permet d'ajouter des commandes à la liste d'attente. */
		void add(std::vector<CMD>);

		/** Fonction qui permet de retirer une commande de la liste d'attente. */
		void remove(int);

		/** Fonction qui permet d'ajouter une commande devant la liste. */
		void put(CMD const&);

		/** Fonction qui permet de la lancer l'exécution séquentiel des commandes de la liste. */
		bool run();

		/** Fonction qui permet de stoper l'exécution des commandes. */
		bool stop();

};






# endif