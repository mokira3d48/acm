# include "freader.h"

# include <iostream>
# include <string>
# include <fstream>

/** Accesseurs */
std::string FileReader::gettext() const { return this->_text; }
std::vector<std::string>& FileReader::gettexts() { return this->_texts; }

/** Constructeur par défaut. */
FileReader::FileReader() {
	this->_text = "";

}

/** Fonction pour lire tous le contenu texte du fichier. */
void FileReader::readall(std::string _src_) {
	std::ifstream file(_src_);

	if (file) {
		std::string line;

		while (std::getline(file, line)) {
			line[line.size() - 1] = ' ';
			_text = _text + line;

		}

		
	}
	else
		std::cout << "[ERROR]\t Reading file failed!" << std::endl;

	file.close();

}

/** Fonction pour lire le contenu ligne par ligne du fichier. */
void FileReader::readlines(std::string _src_) {
	std::ifstream file(_src_);

	std::string s("");

	if (file) {
		std::string line;

		while (std::getline(file, line)) {
			_texts.push_back(line);

		}


	}
	else
		std::cout << "[ERROR]\t Reading file failed!" << std::endl;

	file.close();

}










