# ifndef CMDEXT_H
# define CMDEXT_H

# include <string>
# include <vector>

# include "cmd.h"

class CMDExtractor {
	private :
		std::vector<CMD> _commandes;		// Liste des commandes extraites.



	public :

		/** Accesseur. */
		std::vector<CMD>& getcommandes();


		/** Fonction qui permet d'extraire les commandes d'une liste de chaine de caractères. */
		void extract(std::vector<std::string>);

};


# endif