# include "cmdexec.h"

# include <iostream>
# include <string>
# include <vector>
# include <cstdlib>
# include <cstdio>


/** Accesseur. */
std::vector<CMD> CMDExecutor::getcmdQueue() { return this->_cmds; }
std::vector<CMD> CMDExecutor::getcmdDone() { return this->_ecmds; }

/** Fonction qui permet d'ajouter une commande à la liste d'attente. */
void CMDExecutor::add(CMD const& _cmd_) {
	_cmds.push_back(_cmd_);

}


/** Fonction qui permet d'ajouter une commande à la liste d'attente. */
void CMDExecutor::add(std::vector<CMD> _cmds_) {
	std::vector<CMD>::iterator it;

	for (it = _cmds_.begin(); it != _cmds_.end(); it++)
		_cmds.push_back(*it);

}

/** Fonction qui permet de retirer une commande de la liste d'attente. */
void CMDExecutor::remove(int _id_) {
	if (_id_ >= 0 && _id_ < _cmds.size()) {
		int i = 0;
		std::vector<CMD>::iterator it = _cmds.begin();

		while (i < _id_) 
			it++;

		_cmds.erase(it);
	}

}

/** Fonction qui permet d'ajouter une commande devant la liste. */
void CMDExecutor::put(CMD const& _cmd_) {
	std::vector<CMD>::iterator it = _cmds.begin();
	_cmds.insert(it, _cmd_);

}

/** Fonction qui permet de la lancer l'exécution séquentiel des commandes de la liste. */
bool CMDExecutor::run() {
	_run = true;

	while (_run && !_cmds.empty()) {
		CMD cmd = _cmds.front();
		_cmds.erase(_cmds.begin());

		const char * str = std::string(cmd.text()).c_str();
		printf("%s\n", str);
		system(str);

		_ecmds.push_back(cmd);
	}

	return true;
}

/** Fonction qui permet de stoper l'exécution des commandes. */
bool CMDExecutor::stop() {
	_run = false;
	return true;

}













