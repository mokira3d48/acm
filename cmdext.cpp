# include "cmdext.h"

# include <string>
# include <vector>


/** Accesseur. */
std::vector<CMD>& CMDExtractor::getcommandes() { return this->_commandes; }

/** Fonction qui permet d'extraire les commandes d'une liste de chaine de caractères. */
void CMDExtractor::extract(std::vector<std::string> strs) {
	for (int i = 0; i < strs.size(); i++)
		_commandes.push_back(CMD(strs[i]));

}