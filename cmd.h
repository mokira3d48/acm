# ifndef CMD_H
# define CMD_H

# include <string>
# include <vector>
#include <algorithm>
#include <iterator>


class CMD {
	private :
		std::string					_text;		// Le texte de la ligne de commande.
		std::string 				_program;	// Le nom du programme.
		std::vector<std::string> 	_arguments;	// La liste des arguments passés au programme.

		/** Fonction qui permet de casser les chaines de caractères en plusieurs chaines. */
		template <class Container>
		void split(const std::string& str, Container& cont, const std::string& delims = " ") {
		    std::size_t current; 
		    std::size_t previous = 0;
		    current = str.find_first_of(delims);

		    while (current != std::string::npos) {
		        cont.push_back(str.substr(previous, current - previous));
		        previous = current + 1;
		        current  = str.find_first_of(delims, previous);
		    }

		    cont.push_back(str.substr(previous, current - previous));
		}

	public :
		/** Accesseur. */
		std::string text()	  const;
		std::string program() const;
		std::vector<std::string>& arguments();


		/** Muttateur. */
		void program(std::string);

		/** Constructor. */
		CMD(std::string);


};




# endif