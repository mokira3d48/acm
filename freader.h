# ifndef FREADER_H
# define FREADER_H

# include <string>
# include <vector>

class FileReader {
	private :
		std::string 				_text;	// Le dernier texte lu depuis un fichier.
		std::vector<std::string> 	_texts;	// Les dernières lignes de texte lues depuis un fichier.

	public :
		/** Accesseurs */
		std::string gettext() const;
		std::vector<std::string>& gettexts();

		/** Constructeur par défaut. */
		FileReader();

		/** Fonction pour lire tous le contenu texte du fichier. */
		void readall(std::string);

		/** Fonction pour lire le contenu ligne par ligne du fichier. */
		void readlines(std::string);

};





#endif