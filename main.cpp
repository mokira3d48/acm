# include <iostream>
# include <string>
# include <cstdlib>

# include "freader.h"
# include "cmd.h"
# include "cmdext.h"
# include "cmdexec.h"

int main(int argc, char** argv) {
//	CMD cmd("sudo ln -s /opt/android/studio.sh /usr/bin/android");
//	CMD cmd("");

//	std::cout << cmd.program() << std::endl;

//	for (int i = 0 ; i < cmd.arguments().size(); i++)
//		std::cout << cmd.arguments()[i] << std::endl;

	// On lit les fichiers contenant des commandes :
	FileReader f;
	f.readlines("./asset/cmd1");

	// On extrait les commandes :
	CMDExtractor ce;
	ce.extract(f.gettexts());

	// On affiche les commandes :
	std::vector<CMD> cmds = ce.getcommandes();

	for (int i = 0 ; i < cmds.size(); i++){
		std::cout << cmds[i].program() << std::endl;

		for (int j = 0 ; j < cmds[i].arguments().size(); j++)
			std::cout << cmds[i].arguments()[j] << std::endl;

		std::cout << std::endl;
	}

	// On exécute les commandes :
	CMDExecutor console;
	console.add(cmds);
	console.run();

	for (int i = 0 ; i < console.getcmdDone().size(); i++)
		std::cout << console.getcmdDone()[i].text() << std::endl;

//	std::string cmd("ls -l");

//	system(cmd.c_str());

	return 0;
}